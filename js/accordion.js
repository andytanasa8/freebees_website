"use strict";
function accordion(container) {
  const chevron = container.querySelector(".accordion-chevron");
  const content = container.querySelector(".accordion-content");
  content.style.maxHeight =
    Array.from(content.children).reduce(
      (acc, currentItem) => acc + currentItem.offsetHeight,
      0
    ) + "px";
  loadEvents();
  function loadEvents() {
    chevron.addEventListener("click", openContent);
  }

  function openContent() {
    content.parentNode.classList.toggle("accordion--closed");
  }
}

for (const item of document.querySelectorAll(".accordion")) {
  accordion(item);
}
