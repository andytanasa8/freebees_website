"use strict";
function slider(container) {
  const chevronLeft = container.querySelector(".chevron-left");
  const chevronRight = container.querySelector(".chevron-right");
  const slides = Array.from(container.querySelectorAll(".card-content"));
  const cardContentInner = container.querySelector(".card-container__inner");
  let currentIndex = 0;
  let currentOffSetLeft = 0;
  //load Events
  loadEvents();
  function loadEvents() {
    chevronLeft.addEventListener("click", moveLeft);
    chevronRight.addEventListener("click", moveRight);
  }

  function moveLeft() {
    const currentSlide = slides[currentIndex];
    let offSetLeft;
    if (
      currentSlide.offsetWidth <
      cardContentInner.offsetWidth - container.offsetWidth - currentOffSetLeft
    ) {
      offSetLeft = -(currentOffSetLeft + currentSlide.offsetWidth);
      cardContentInner.style.transform = `translateX(${offSetLeft}px)`;
    } else {
      offSetLeft = -(cardContentInner.offsetWidth - container.offsetWidth);
      cardContentInner.style.transform = `translateX(${offSetLeft}px)`;
    }
    if (currentIndex < slides.length - 1) {
      currentIndex++;
    }
    console.log(currentIndex);
    currentOffSetLeft = -offSetLeft;
  }
  function moveRight() {
    const currentSlide = slides[currentIndex];
    let offSetLeft;
    if (currentSlide.offsetWidth < currentOffSetLeft) {
      offSetLeft = -(currentOffSetLeft - currentSlide.offsetWidth);
      cardContentInner.style.transform = `translateX(${offSetLeft}px)`;
    } else {
      offSetLeft = 0;
      cardContentInner.style.transform = `translateX(${offSetLeft}px)`;
    }
    if (currentIndex > 0) {
      currentIndex--;
    }
    // console.log(currentIndex);
    currentOffSetLeft = -offSetLeft;
  }
}
slider(document.querySelector("#sliderToLeft"));
slider(document.querySelector("#sliderToRight"));
