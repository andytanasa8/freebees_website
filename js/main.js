"use strict";
const hamburgerMenuBtnOpen = document.querySelector(".hamburger-icon__open");
const hamburgerMenuBtnClose = document.querySelector(".hamburger-icon__close");
const hamburgerMenu = document.querySelector(".hamburger-menu");

loadEventListeners();

function loadEventListeners() {
  window.addEventListener("load", checkDefault);
  window.addEventListener("resize", checkWidth);
  window.document
    .querySelectorAll(".btn")
    .forEach((value) => value.addEventListener("click", addSpinner));
  hamburgerMenuBtnOpen.addEventListener("click", openMenu);
  hamburgerMenuBtnClose.addEventListener("click", closeMenu);
}
function checkDefault() {
  hamburgerMenu.classList.add("d-none");
}
function checkWidth() {
  const windowWidth = window.innerWidth;

  hamburgerMenu.classList.add("d-none");
  hamburgerMenuBtnOpen.classList.add("d-none");
  hamburgerMenuBtnOpen.classList.remove("d-block");
  hamburgerMenuBtnClose.classList.add("d-none");
  hamburgerMenuBtnClose.classList.remove("d-block");
  if (windowWidth <= 959) {
    hamburgerMenuBtnOpen.classList.add("d-block");
    hamburgerMenu.classList.add("d-none");
    hamburgerMenu.classList.remove("d-block");
  } else {
    hamburgerMenuBtnOpen.classList.add("d-none");
  }
}

function openMenu() {
  hamburgerMenuBtnOpen.classList.add("d-none");
  hamburgerMenu.classList.add("d-block");
  hamburgerMenu.classList.remove("d-none");
  hamburgerMenuBtnOpen.classList.remove("d-block");
  hamburgerMenuBtnClose.classList.add("d-block");
}

function closeMenu() {
  hamburgerMenuBtnClose.classList.add("d-none");
  hamburgerMenuBtnClose.classList.remove("d-block");
  hamburgerMenuBtnOpen.classList.add("d-block");
  hamburgerMenu.classList.add("d-none");
  hamburgerMenu.classList.remove("d-block");
}

function addSpinner(e) {
  e.target.classList.add("button--loading");
  setTimeout(() => {
    e.target.classList.remove("button--loading");
  }, 3000);
}
